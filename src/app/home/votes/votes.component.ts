import {Component, OnInit} from '@angular/core';
import {VoteService} from '../../services/vote.service';
import {VoteModel} from '../../model/vote.model';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup} from '@angular/forms';

@Component({
    selector: 'app-votes',
    templateUrl: './votes.component.html',
    styleUrls: ['./votes.component.css'],
    providers: [VoteService]
})
export class VotesComponent implements OnInit {

    votes: VoteModel[];
    formGroup: FormGroup;

    constructor(private formBuilder: FormBuilder, private voteService: VoteService, private router: Router) {
    }

    ngOnInit(): void {
        this.voteService.getRecords().subscribe(value => {
            this.votes = value;
        });
        this.formGroup = this.formBuilder.group({
            conceptName: ['']
        });
    }

    vota(idconcepto: number) {
        this.voteService.vote(idconcepto).subscribe(value => {
            this.voteService.getRecords().subscribe(v => {
                this.votes = v;
            });
        });
    }

    submitForm() {
        console.log('Se esta enviando el formulario');
        this.voteService.saveConcept(this.formGroup.value.conceptName).subscribe(value => {
            this.voteService.getRecords().subscribe(v => {
                this.votes = v;
            });
        });
    }

}
