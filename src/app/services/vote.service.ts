import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {ConceptRequestModel} from '../model/concept.request.model';
import {VoteRequestModel} from '../model/vote.request.model';

@Injectable({
    providedIn: 'root'
})
export class VoteService {
    constructor(private http: HttpClient) {
    }

    configUrl = environment.apiurl + '/v1/vote';

    getRecords(): Observable<any> {
        return this.http.get(this.configUrl);
    }

    saveConcept(concept: string): Observable<any> {
        const url = this.configUrl + '/register';
        const conceptRequest: ConceptRequestModel = {
            conceptName: concept
        };
        return this.http.post(url, conceptRequest);
    }

    vote(conceptId: number): Observable<any> {
        const url = this.configUrl + '/vote';
        const voteRequest: VoteRequestModel = {
            idConcept: conceptId
        };
        return this.http.post(url, voteRequest);
    }

}
